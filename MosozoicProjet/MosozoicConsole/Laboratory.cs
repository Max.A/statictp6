﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MosozoicConsole
{
    public class Laboratory
    {
        public static Dinosaur CreateDinosaur(string name, string specie)
        {
            return new Dinosaur(name, specie, 0);
        }
    }
}
