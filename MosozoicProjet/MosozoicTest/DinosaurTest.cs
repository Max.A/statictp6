﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MosozoicConsole;

namespace MosozoicTest
{
    public class DinosaurTest
    {
        [TestMethod]
        public void TestDinosaurConstructor()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);

            Assert.AreEqual("Louis", louis.GetName());
            Assert.AreEqual("Stegausaurus", louis.GetSpecie());
            Assert.AreEqual(12, louis.GetAge());
        }

        [TestMethod]
        public void TestDinosaurRoar()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.AreEqual("Grrr", louis.roar());
        }

        [TestMethod]
        public void TestDinosaurSayHello()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            Dinosaur bob = new Dinosaur("Bob", "Raptor Jesus", 24);
            Dinosaur sully = new Dinosaur("Sully", "T-rex", 7);
            Assert.AreEqual("Je suis Louis le Stegausaurus, j'ai 12 ans.", louis.sayHello());
            Assert.AreEqual("Je suis Nessie le Diplodocus, j'ai 11 ans.", nessie.sayHello());
            Assert.AreEqual("Je suis Bob le Raptor Jesus, j'ai 24 ans.", bob.sayHello());
            Assert.AreEqual("Je suis Sully le T-rex, j'ai 7 ans.", sully.sayHello());
        }

        [TestMethod]
        public void TestDinosaurHug()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);

            Assert.AreEqual("Je suis Louis et je fais un câlin à Nessie.", louis.Hug(nessie));
            Assert.AreEqual("Je suis Nessie et je fais un câlin à Louis.", nessie.Hug(louis));
            Assert.AreEqual("Je suis Nessie et je ne peux pas me faire de câlin à moi-même :'(.", nessie.Hug(nessie));
        }
    }
}
