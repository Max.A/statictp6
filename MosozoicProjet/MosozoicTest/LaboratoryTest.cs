﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MosozoicConsole;

namespace MosozoicTest
{
    [TestClass]
    public class LaboratoryTest
    {
        [TestMethod]
        public void TestCreateDinosaur()
        {
            Dinosaur dinosaur = Laboratory.CreateDinosaur("Nessie", "Diplodocus");
            Assert.IsNotNull(dinosaur);
            Assert.AreEqual("Nessie", dinosaur.GetName());
            Assert.AreEqual("Diplodocus", dinosaur.GetSpecie());
            Assert.AreEqual(0, dinosaur.GetAge());
        }
    }
}
